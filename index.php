<?php

require __dir__ . '/vendor/autoload.php';

$appState = new \ModernWays\AnOrmApart\NoticeBoard();
$route = new \ModernWays\Mvc\Route($appState, 'Admin-Index');
$routeConfig = new \ModernWays\Mvc\RouteConfig('\ModernWays\Webshop', $route, $appState);
$view = $routeConfig->invokeActionMethod();
?>
<head>
    <title>Webwinkel</title>
    <meta charset="UTF-8"/>
    <meta name="author" content="Nicky Fasoel en Alek Van Tichelen"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/main.css"/>
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
</head>
<body>
    <header>
        <form action="index.php" method="post">
            <input type="text" name="search" placeholder="orders zoeken op klantnaam" />
            <button value="Order-search" name="uc">Zoek</button>
            <a href="index.php">
                Mikmak
            </a>
        </form>
    </header>
    <?php call_user_func($view); ?>
    <footer>
        Nicky en Alek Project Webwinkel PHP <?php echo date("Y"); ?>
    </footer>
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>
