<?php
require ('../autoload.php');
// instanciate feedback class
// make an object of the Feedback class
// object maken van de Feedback klasse
$notice = new \ModernWays\Dialog\Model\Notice();
$view = function ($model) {
    include("../src/View/Notice.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Feedback test</title>
</head>
<body>
<?php
$notice->setText('Feedback');
$notice->setCode('000');
$notice->setCaption('Feedback caption');
$notice->setCodeDriver('Feedback errorCodeDriver');
$notice->setText('Type');
$notice->setName('tests');
$notice->setStartTime();
$notice->setEndTime();
?>
<h1>De getters en setters testen</h1>
<?php $view($notice);?>

<!-- gebruiksprocedure -->
<?php
// de Start methode initialiseert de naam,
// registreert de starttijd en zet
// alle andere waarden op de standaardwaarde
$notice->Start('Plopperdeplop');
?>
<?php $view($notice);?>

</body>
</html>

