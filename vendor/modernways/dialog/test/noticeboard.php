<?php
require ('../autoload.php');
// instanciate feedback class
// make an object of the Feedback class
// object maken van de Feedback klasse
$noticeBoard = new \ModernWays\Dialog\Model\NoticeBoard();
$view = function ($model, $viewPath) {
    include("{$viewPath}NoticeBoard.php");
};

$noticeBoard->setTitle('De getters en setters testen');
$noticeBoard->setText('Feedback text');
$noticeBoard->setCode('000');
$noticeBoard->setCaption('Feedback caption');
$noticeBoard->setCodeDriver('Feedback errorCodeDriver');
$noticeBoard->setType('Type');
$noticeBoard->setName('tests');
$noticeBoard->setStartTime();
$noticeBoard->end();

// de Start methode initialiseert de naam,
// registreert de starttijd en zet
// alle andere waarden op de standaardwaarde
$noticeBoard->setTitle('De startmethode testen');
$noticeBoard->Start('Plopperdeplop');
$noticeBoard->setText('Feedback errormessage Plopperdeplop');
$noticeBoard->setCodeDriver('Feedback errorCodeDriver Plopperdeplop');
$noticeBoard->end();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Notice board test</title>
</head>
<body>
<?php $view($noticeBoard, "../src/View/");?>

</body>
</html>

