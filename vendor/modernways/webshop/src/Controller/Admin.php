<?php

namespace ModernWays\Webshop\Controller;

class Admin extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        return $this->view('Admin', 'Index', null);
    }
}