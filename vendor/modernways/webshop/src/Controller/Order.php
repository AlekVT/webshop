<?php

namespace ModernWays\Webshop\Controller;

class Order extends \ModernWays\Mvc\Controller
{
    private $model;
    private $dal;
    private $provider;
    
    private function getForeignKey($foreignKey, $id = NULL)
    {
        $modelClass = '\ModernWays\Webshop\Model\\' . $foreignKey;
        $fkModel = new $modelClass($this->noticeBoard);
        
        $dalClass = '\ModernWays\Webshop\Dal\\' . $foreignKey;
        $fkDal = new $dalClass($fkModel, $this->provider);
        
        $fkModel->setId($id);
        
        if ($id !== NULL) {
            $fkDal->readingOne();
        } else {
            $fkDal->readingAll();
        }
        
        return $fkModel;
    }
    
    private function setVariables()
    {
        $this->model->setOrderDate(filter_input(INPUT_POST, 'Order-OrderDate', FILTER_SANITIZE_STRING));
        $this->model->setShippingDate(filter_input(INPUT_POST, 'Order-ShippingDate', FILTER_SANITIZE_STRING));
        $this->model->setComment(filter_input(INPUT_POST, 'Order-Comment', FILTER_SANITIZE_STRING));
        $this->model->setId(filter_input(INPUT_POST, 'Order-Id', FILTER_SANITIZE_STRING));
        $this->model->setIdCustomer(filter_input(INPUT_POST, 'Order-IdCustomer', FILTER_SANITIZE_STRING));
        $this->model->setIdShippingMethod(filter_input(INPUT_POST, 'Order-IdShippingMethod', FILTER_SANITIZE_STRING));
        $this->model->setIdStatus(filter_input(INPUT_POST, 'Order-IdStatus', FILTER_SANITIZE_STRING));
    }

    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\NoticeBoard $noticeBoard = null) {
        parent::__construct($route, $noticeBoard);
        $this->model = new \ModernWays\Webshop\Model\Order($this->noticeBoard);
        $this->provider = new \ModernWays\AnOrmApart\Provider('WebwinkelOVH', $this->noticeBoard);
        $this->dal = new \ModernWays\Webshop\Dal\Order($this->model, $this->provider);
    }
    
    public function creatingOne()
    {
        $this->dal->readingAll();
        
        $this->model->setCustomers($this->getForeignKey('Customer')->getList());
        $this->model->setShippingList($this->getForeignKey('ShippingMethod')->getList());
        $this->model->setStatusList($this->getForeignKey('OrderStatus')->getList());
        
        return $this->view('Order', 'CreatingOne', $this->model);
    }

    public function createOne()
    {
        $this->setVariables();
        $this->dal->createOne();
        $this->dal->readingAll();
        
        $orderItem = $this->getForeignKey('OrderItem');
        $orderItem->setProducts($this->getForeignKey('Product')->getList());
        
        $list = $this->model->getList();
        $orderItem->setIdOrder($list[count($list) -1]['Id']);
        
        return $this->view('OrderItem', 'CreatingOne', $orderItem);
    }

    public function editing()
    {
        $this->dal->readingAll();
        return $this->view('Order', 'Editing', $this->model);
    }

    public function readingOne()
    {
        $this->model->setId($this->route->getId());
        $this->dal->readingOne();
        $this->dal->readingAll();
        
        $this->model->setCustomerName($this->getForeignKey('Customer', $this->model->getIdCustomer())->getLastname());
        $this->model->setShippingName($this->getForeignKey('ShippingMethod', $this->model->getIdShippingMethod())->getName());
        $this->model->setStatusName($this->getForeignKey('OrderStatus', $this->model->getIdStatus())->getName());
        $this->model->setOrderItems($this->getForeignKey('OrderItem')->getList());
        
        return $this->view('Order', 'ReadingOne', $this->model);
    }

    public function updatingOne()
    {
        if ($this->route->getId() !== NULL) {
            $this->model->setId($this->route->getId());
        } else {
            $this->model->setId(filter_input(INPUT_POST, 'Order-Id', FILTER_SANITIZE_NUMBER_INT));
        }
        
        $this->dal->readingOne();
        $this->dal->readingAll();
        $this->model->setCustomers($this->getForeignKey('Customer')->getList());
        $this->model->setShippingList($this->getForeignKey('ShippingMethod')->getList());
        $this->model->setStatusList($this->getForeignKey('OrderStatus')->getList());
        $this->model->setOrderItems($this->getForeignKey('OrderItem')->getList());
        
        return $this->view('Order', 'UpdatingOne', $this->model);
    }

    public function deleteOne()
    {
        $this->model->setId(filter_input(INPUT_POST, 'Order-Id', FILTER_SANITIZE_NUMBER_INT));
        $this->dal->deleteOne();
        $this->dal->readingAll();
        return $this->view('Order', 'Editing', $this->model);
    }

    public function updateOne()
    {
        $this->setVariables();
        $this->dal->updateOne();
        $this->dal->readingAll();
        // read modified record again
        $this->dal->readingOne();
        $this->model->setCustomerName($this->getForeignKey('Customer', $this->model->getIdCustomer())->getLastname());
        $this->model->setShippingName($this->getForeignKey('ShippingMethod', $this->model->getIdShippingMethod())->getName());
        $this->model->setStatusName($this->getForeignKey('OrderStatus', $this->model->getIdStatus())->getName());
        $this->model->setOrderItems($this->getForeignKey('OrderItem')->getList());
        return $this->view('Order', 'ReadingOne', $this->model);
    }
    
    public function search()
    {
        $this->model->setCustomerName(filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING));
        $this->dal->search();
        
        return $this->view('Order', 'Editing', $this->model);
    }
}
