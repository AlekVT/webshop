<?php
namespace ModernWays\Webshop\Controller;

class OrderItem extends \ModernWays\Mvc\Controller
{
    private $model;
    private $dal;
    private $provider;
    
    private function getForeignKey($foreignKey, $id = NULL, $both = FALSE)
    {
        $modelClass = '\ModernWays\Webshop\Model\\' . $foreignKey;
        $fkModel = new $modelClass($this->noticeBoard);
        
        $this->dalClass = '\ModernWays\Webshop\Dal\\' . $foreignKey;
        $fkDal = new $this->dalClass($fkModel, $this->provider);
        
        $fkModel->setId($id);
        
        if ($both === TRUE) {
            $fkDal->readingOne();
            $fkDal->readingAll();
        } elseif ($id !== NULL) {
            $fkDal->readingOne();
        } else {
            $fkDal->readingAll();
        }
        
        return $fkModel;
    }
    
    private function getOrderModel($readAll = FALSE)
    {
        $orders = $this->getForeignKey('Order', $this->model->getIdOrder(), $readAll);
        $orders->setCustomers($this->getForeignKey('Customer')->getList());
        $orders->setShippingList($this->getForeignKey('ShippingMethod')->getList());
        $orders->setStatusList($this->getForeignKey('OrderStatus')->getList());
        $orders->setOrderItems($this->getForeignKey('OrderItem')->getList());
        return $orders;
    }

    private function setVariables()
    {
        $this->model->setId(filter_input(INPUT_POST, 'OrderItem-Id', FILTER_SANITIZE_NUMBER_INT));
        $this->model->setQuantity(filter_input(INPUT_POST, 'OrderItem-Quantity', FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));
        $this->model->setIdOrder(filter_input(INPUT_POST, 'OrderItem-IdOrder', FILTER_SANITIZE_NUMBER_INT));
        $this->model->setIdProduct(filter_input(INPUT_POST, 'OrderItem-IdProduct', FILTER_SANITIZE_NUMBER_INT));
    }

        public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\NoticeBoard $noticeBoard = null) {
        parent::__construct($route, $noticeBoard);
        $this->model = new \ModernWays\Webshop\Model\OrderItem($this->noticeBoard);
        $this->provider = new \ModernWays\AnOrmApart\Provider('WebwinkelOVH', $this->noticeBoard);
        $this->dal = new \ModernWays\Webshop\Dal\OrderItem($this->model, $this->provider);
    }

        public function creatingOne()
    {
        $this->dal->readingAll();
        
        if ($this->route->getId() !== NULL) {
            $this->model->setIdOrder($this->route->getId());
        }
        else {
            $this->model->setOrders($this->getForeignKey('Order')->getList());
        }
        
        $this->model->setProducts($this->getForeignKey('Product')->getList());
        
        return $this->view('OrderItem', 'CreatingOne', $this->model);
    }

    public function createOne()
    {
        $this->setVariables();
        $this->dal->createOne();
        // Refresh list
        $this->dal->readingAll();
        return $this->view('OrderItem', 'Editing', $this->model);
    }
    
    public function createNext()
    {
        $this->setVariables();
        $orderId = $this->model->getIdOrder();
        $this->dal->createOne();
        
        $this->model = new \ModernWays\Webshop\Model\OrderItem($this->noticeBoard);
        $this->model->setProducts($this->getForeignKey('Product')->getList());
        $this->model->setIdOrder($orderId);
        $this->dal->readingAll();
        
        return $this->view('OrderItem', 'CreatingOne', $this->model);
    }

    public function editing()
    {
        $this->dal->readingAll();
        
        $this->model->setProducts($this->getForeignKey('Product')->getList());
        
        return $this->view('OrderItem', 'Editing', $this->model);
    }

    public function readingOne()
    {
        $this->model->setId($this->route->getId());
        $this->dal->readingOne();
        $this->dal->readingAll();
        
        $this->model->setProductName($this->getForeignKey('Product', $this->model->getIdProduct())->getName());
        
        return $this->view('OrderItem', 'ReadingOne', $this->model);
    }

    public function updatingOne()
    {
        if ($this->route->getId() !== NULL) {
            $this->model->setId($this->route->getId());
        } else {
            $this->model->setId(filter_input(INPUT_POST, 'OrderItem-Id', FILTER_SANITIZE_NUMBER_INT));
        }
        $this->dal->readingOne();
        $this->dal->readingAll();
        
        $this->model->setOrders($this->getForeignKey('Order')->getList());
        $this->model->setProducts($this->getForeignKey('Product')->getList());
        
        if ($this->route->getId()) {
            return $this->view('OrderItem', 'UpdatingOne', $this->model);
        }
        return $this->view('OrderItem', 'UpdatingOne', $this->model);
    }

    public function deleteOne()
    {
        $id = $this->route->getId();
        if ($id !== NULL) {
            $this->model->setId($id);
            $this->dal->readingOne();
        } else {
            $this->model->setId(filter_input(INPUT_POST, 'OrderItem-Id', FILTER_SANITIZE_NUMBER_INT));
        }
        
        $this->dal->readingOne();
        $this->dal->deleteOne();
        
        if ($id !== NULL) {
            return $this->view('Order','UpdatingOne', $this->getOrderModel(TRUE));
        } else {
            $this->dal->readingAll();
            return $this->view('OrderItem', 'Editing', $this->model);
        }
    }

    public function updateOne()
    {
        $this->setVariables();
        $this->dal->updateOne();
        $this->dal->readingAll();
        // read modified record again
        $this->dal->readingOne();
        $this->model->setProductName($this->getForeignKey('Product', $this->model->getIdProduct())->getName());
        return $this->view('OrderItem', 'ReadingOne', $this->model);
    }
}