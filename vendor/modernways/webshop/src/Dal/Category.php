<?php

namespace ModernWays\Webshop\Dal;
class Category extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\Webshop\Model\Category $model,
                                \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }
}
