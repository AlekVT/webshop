<?php

namespace ModernWays\Webshop\Dal;
class Order extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\Webshop\Model\Order $model,
                                \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }
    
    public function search()
    {
        $result = false;
        if (!$this->provider->isConnected()) {
            $this->provider->open();
        }
        if ($this->provider->isConnected()) {
            try {
                // Prepare stored procedure call
                $preparedStatement = $this->provider->getPdo()->
                prepare("CALL OrderSelectByName(:pName)");
                // so we cannot use bindParam that requires a variable by value
                // if you want to use a variable, use then bindParam
                $preparedStatement->bindValue(':pName', $this->model->getCustomerName(), \PDO::PARAM_STR);
                $preparedStatement->execute();
                
                $this->rowCount = $preparedStatement->rowCount();
                $this->model->setList($preparedStatement->fetchAll());
                if ($this->model->getList()) {
                    $this->model->getModelState()->crudNotice('Search', true, $this,
                        null, $preparedStatement->errorInfo());
                } else {
                    $this->model->getModelState()->crudNotice('Search', false, $this,
                        null, $preparedStatement->errorInfo());
                }
            } catch (\PDOException $e) {
                $this->model->getModelState()->crudNotice('Search', false, $this, $e);
            }
        } else {
            $this->model->getModelState()->notconnected($this->provider);
        }
        return $result;
    }
}
