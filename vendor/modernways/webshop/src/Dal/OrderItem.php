<?php

namespace ModernWays\Webshop\Dal;
class OrderItem extends \ModernWays\AnOrmApart\Dal
{
    public function __construct(\ModernWays\Webshop\Model\OrderItem $model,
                                \ModernWays\AnOrmApart\Provider $provider)
    {
        $this->model = $model;
        parent::__construct($provider);
    }
}