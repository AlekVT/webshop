<?php

namespace ModernWays\Webshop\Model;

class Customer extends \ModernWays\Mvc\Model
{
    private $id;
    private $lastName;
    
    public function setLastname($name) {
        $this->lastName = $name;
    }

    public function getLastname() {
        return $this->lastName;
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getId() {
        return $this->id;
    }
}