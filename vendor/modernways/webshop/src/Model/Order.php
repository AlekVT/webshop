<?php

namespace ModernWays\Webshop\Model;

class Order extends \ModernWays\Mvc\Model
{
    private $orderDate;
    private $shippingDate;
    private $comment;
    private $id;
    private $idCustomer;
    private $idShippingMethod;
    private $idStatus;
    protected $customerName;
    protected $shippingName;
    protected $statusName;
    protected $customerList;
    protected $shippingMethodList;
    protected $statusList;
    protected $orderItems;


    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param mixed $orderDate
     */
    public function setOrderDate($orderDate)
    {
        if($this->validRequired($orderDate, "OrderDate"))
        {
            $this->orderDate = $orderDate;
        }
    }

    /**
     * @return mixed
     */
    public function getShippingDate()
    {
        return $this->shippingDate;
    }

    /**
     * @param mixed $shippingDate
     */
    public function setShippingDate($shippingDate)
    {
        if($this->validRequired($shippingDate, "ShippingDate"))
        {
            $this->shippingDate = $shippingDate;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * @return mixed
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * @param mixed $idCustomer
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;
    }
    
    
    /**
     * @return mixed
     */
    public function getIdShippingMethod()
    {
        return $this->idShippingMethod;
    }

    /**
     * @param mixed $idShippingMethod
     */
    public function setIdShippingMethod($idShippingMethod)
    {
        $this->idShippingMethod = $idShippingMethod;
    }
    
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * @param mixed $idStatus
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;
    }
    
    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * @param mixed $comment
     */
    public function setComment($comment) 
    {
        $this->comment = $comment;
    }
    
    public function getOrderItems() 
    {
        return $this->orderItems;
    }
    
    public function setOrderItems($orderItems)
    {
        $this->orderItems = $orderItems;
    }
    
    public function getCustomerName() 
    {
        return $this->customerName;
    }
    
    public function setCustomerName($customerName) 
    {
        $this->customerName = $customerName;
    }
    
    public function getShippingName()
    {
        return $this->shippingName;
    }
    
    public function setShippingName($shippingName)
    {
        $this->shippingName = $shippingName;
    }
    
    public function setStatusName($statusName)
    {
        $this->statusName = $statusName;
    }
    
    public function getStatusName()
    {
        return $this->statusName;
    }
    
    public function getCustomers()
    {
        return $this->customerList;
    }
    
    public function setCustomers($customers)
    {
        $this->customerList = $customers;
    }
    
    public function getShippingList()
    {
        return $this->shippingMethodList;
    }
    
    public function setShippingList($shipping)
    {
        $this->shippingMethodList = $shipping;
    }
    
    public function getStatusList()
    {
        return $this->statusList;
    }
    
    public function setStatusList($statusList)
    {
        $this->statusList = $statusList;
    }
}