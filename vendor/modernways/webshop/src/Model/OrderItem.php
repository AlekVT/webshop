<?php
/**
 * Created by PhpStorm.
 * User: Alek
 * Date: 01/02/2016
 * Time: 14:48
 */
namespace ModernWays\Webshop\Model;

class OrderItem extends \ModernWays\Mvc\Model
{
    private $idProduct;
    private $idOrder;
    private $id;
    private $quantity;
    protected $orders;
    protected $productList;
    protected $productName;

    public function getIdProduct()
    {
        return $this->idProduct;
    }

    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    }

    public function getIdOrder()
    {
        return $this->idOrder;
    }

    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    
    public function setOrders($orders) {
        $this->orders = $orders;
    }
    
    public function getOrders() {
        return $this->orders;
    }
    
    public function setProducts($products) {
        $this->productList = $products;
    }
    
    public function getProducts() {
        return $this->productList;
    }
    
    public function setProductName($name) {
        $this->productName = $name;
    }
    
    public function getProductName() {
        return $this->productName;
    }
}