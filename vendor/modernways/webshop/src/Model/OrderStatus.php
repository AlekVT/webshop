<?php

namespace ModernWays\Webshop\Model;

class OrderStatus extends \ModernWays\Mvc\Model
{
    private $id;
    private $name;
    
    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getId() {
        return $this->id;
    }
}

