<div id="tilecontainer">
    <nav id="admin">
        <a href="index.php?uc=Customer-editing" class="tile">
            <div class="tileimage" id="custimage">
                
            </div>
            <p>
                Customer
            </p>
        </a>

        <a href="index.php?uc=Supplier-editing" class="tile">
            <div class="tileimage" id="suppimage">
                
            </div>
            <p>
                Supplier
            </p>
        </a>
        
        <a href="index.php?uc=Product-editing" class="tile">
            <div class="tileimage" id="prodimage">
                
            </div>
            <p>
                Product
            </p>
        </a>
        
        <a href="index.php?uc=Order-editing" class="tile">
            <div class="tileimage" id="orderimage">
                
            </div>
            <p>
                Order
            </p>
        </a>
        <a href="index.php?uc=Country-editing" class="tile">
            <div class="tileimage" id="countryimage">
                
            </div>
            <p>
                Country
            </p>
        </a>

        <a href="index.php?uc=Category-editing" class="tile">
            <div class="tileimage" id="catimage">
                
            </div>
            <p>
                Category
            </p>
        </a>
        <a href="index.php?uc=UnitBase-editing" class="tile">
            <div class="tileimage" id="unitimage">
                
            </div>
            <p>
                Unit Base
            </p>
        </a>
        <a href="index.php?uc=OrderStatus-editing" class="tile">
            <div class="tileimage" id="statimage">
                
            </div>
            <p>
                Order Status
            </p>
        </a>
    </nav>
</div>
