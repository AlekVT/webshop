<article>
    
    <form action="Index.php" method="post">
        <nav>
            <h1>Order CreatingOne</h1>
            <div id="controls">
                <button type="submit" value="Order-createOne" name="uc">
                    Opslaan
                </button>
                <a href="index.php?uc=Order-editing">
                    Sluiten
                </a>
            </div>
        </nav>
        <div id="readingalles">
            <?php $partialView('Order', 'ReadingAll', $model); ?>
        </div>
        
        <fieldset id="content">
            <div>
                <label for="Order-Comment">Opmerking:</label>
                <input id="Order-Comment" name="Order-Comment"
                       class="text" type="text"/>
            </div>
            <div>
                <label for="Order-ShippingDate">Verzenddatum:</label>
                <input type="text" id="Order-ShippingDate" name="Order-ShippingDate"></input>
            </div>
            <div>
                <label for="Order-OrderDate">Besteldatum:</label>
                <input type="text" id="Order-OrderDate" name="Order-OrderDate" value="<?php echo date('Y-m-d H:i:s'); ?>"</input>
            </div>
            <div>
                <label for="Order-IdCustomer">Klant:</label>
                <select id="Order-IdCustomer" name="Order-IdCustomer" required class="text" type="text">
                            <?php
                            if (count($model->getCustomers()) > 0) {
                                foreach ($model->getCustomers() as $item) {
                                    ?><option value="<?php echo $item['Id'];?>"><?php echo $item['LastName'];?></option><?php
                                }
                            } else {
                                ?><option>Geen klanten gevonden.</option><?php
                            } ?>
                </select>
            </div>
            <div>
                <label for="Order-IdShippingMethod">Verzendingsmethode:</label>
                <select id="Order-IdShippingMethod" name="Order-IdShippingMethod" required
                    class="text" type="text">
                        <?php
                        if (count($model->getShippingList()) > 0) {
                            foreach ($model->getShippingList() as $item) {
                                ?><option value="<?php echo $item['Id'];?>"><?php echo $item['Name'];?></option><?php
                            }
                        } else {
                            ?><option>Geen verzendmethodes gevonden.</option><?php
                        } ?>
                </select>
            </div>
            <div>
                <label for="Order-IdStatus">Status:</label>
                <select id="Order-IdStatus" name="Order-IdStatus" required
                    class="text" type="text">
                        <?php
                        if (count($model->getStatusList()) > 0) {
                            foreach ($model->getStatusList() as $item) {
                                ?><option value="<?php echo $item['Id'];?>"><?php echo $item['Name'];?></option><?php
                            }
                        } else {
                            ?><option>Geen status gevonden.</option><?php
                        } ?>
                </select>
            </div>
        </fieldset>
        
    </form>
            <button class="log" onclick="toggle_visibility()">Show Log</button>
    <?php $appStateView(); ?>
</article>


