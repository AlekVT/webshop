<article>
    <form action="index.php" method="post">
        <nav>
            <h1>Order Editing
            <?php if ($model->getCustomerName() !== NULL) {
            ?>: Resultaten voor '<?php echo $model->getCustomerName();?>'<?php }?>
            </h1>
            <div id="controls">
                <button type="submit" value="Order-creatingOne" name="uc">
                    Inserten
                </button>
            </div>
        </nav>
        <div id="readingalles"> 
            <?php $partialView('Order', 'ReadingAll', $model); ?>
        </div>
    </form>
    <button class="log" onclick="toggle_visibility()">Show Log</button>
    <?php $appStateView(); ?>
</article>
