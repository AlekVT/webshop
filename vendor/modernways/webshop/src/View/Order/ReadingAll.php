
    <?php
    if (count($model->getList()) > 0)
    {
        ?>
        <table>
            <tr>
                <th>
                    Select
                </th>
                <th>
                    Verzenddatum
                </th>
                <th>
                    Besteldatum
                </th>
            </tr>
            <?php
            foreach ($model->getList() as $item)
            { 
                ?>
                
                <tr>
                    <td class="handje">
                        <a href="index.php?uc=Order-readingOne_<?php echo $item['Id'];?>">
                            <img src="images/selectIcon.png" class="selectIcon"></a>
                    </td>
                    <td>
                        <?php echo $item['OrderDate'];?>
                    </td>
                    <td>
                        <?php echo $item['ShippingDate'];?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
    else
    {
        ?>
        <p>Geen Order gevonden!</p>
        <?php
    }
    ?>
