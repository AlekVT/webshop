<article>
    <form action="index.php" method="post">
            <nav>
                <h1>Order ReadingOne</h1>
                <div id="controls">
                    <button type="submit" value="Order-updatingOne" name="uc">
                        Updating
                    </button>
                    <button type="submit" value="Order-creatingOne" name="uc">
                        Inserten
                    </button>
                    <button type="submit" value="Order-deleteOne" name="uc">
                        Delete
                    </button>
                    <a href="index.php?uc=Order-editing">
                        Sluiten
                    </a>
                </div>
            </nav>
            <div id="readingalles">
                <?php $partialView('Order', 'ReadingAll', $model); ?>
            </div>
            <fieldset id="content">
                <div>
                    <input id="Order-Id" name="Order-Id" type="hidden" value="<?php echo $model->getId(); ?>" readonly/>
                </div>
                <div>
                    <label for="Order-Comment">Opmerking:</label>
                    <input id="Order-Comment" name="Order-Comment"
                           class="text" type="text" value="<?php echo $model->getComment(); ?>"readonly
                    />
                </div>
                <div>
                    <label for="Order-ShippingDate">Verzenddatum:</label>
                    <input type="text" value="<?php echo $model->getShippingDate(); ?>" id="Order-ShippingDate" name="Order-ShippingDate" readonly>
                        
                    </input>
                </div>
                <div>
                    <label for="Order-OrderDate">Besteldatum:</label>
                    <input id="Order-OrderDate" name="Order-OrderDate" readonly value="<?php echo $model->getOrderDate(); ?>"></input>
                </div>
                <div>
                    <label for="Order-IdCustomer">Klant:</label>
                    <input id="Order-IdCustomer" name="Order-IdCustomer"
                           type="text" value="<?php echo $model->getCustomerName(); ?>" readonly/>
                </div>
                <div>
                    <label for="Order-IdShippingMethod">Verzendingsmethode:</label>
                    <input id="Order-IdShippingMethod" name="Order-IdShippingMethod"
                           type="text" value="<?php echo $model->getShippingName(); ?>" readonly/>
                </div>
                <div>
                    <label for="Order-IdStatus">Status:</label>
                    <input id="Order-IdStatus" name="Order-IdStatus"
                           type="text" value="<?php echo $model->getStatusName(); ?>" readonly/>
                </div>
                <h2>Order Items</h2>
                <div id="readingorderitems">
                    <table>
                <?php 
                    $fullprice = 0;
                    foreach($model->getOrderItems() as $item) 
                    {
                        if ($item['IdOrder'] === $model->getId()) 
                        { 
                            $quantity = $item['Quantity'];
                            $price = $item['Price'];
                            $fullprice = $fullprice + ($price * $quantity);
                        ?>
                        <tr>
                            <td>
                                <?php echo $item['ProductName'];?>
                            </td>
                            <td>
                                <?php echo "$price/" . $item['UnitBaseName'];?>
                            </td>
                            <td>
                                <?php echo "$quantity " . $item['UnitBaseName'];?>
                            </td>
                            <td>
                                <button class="selectbutton" type="submit" value="OrderItem-ReadingOne_<?php echo $item['Id'];?>" name="uc">
                                    Select
                                </button>
                            </td>
                        </tr>
                        <?php
                        }
                    }
                ?>
                    <tr>
                        <td><label>Totaal: <?php echo $fullprice . '$' ?></label></td>
                    </tr>
                    </table>
                </div>
            </fieldset>
    </form>
    <button id="log" class="log" onclick="toggle_visibility();">Show Log</button>
    <?php $appStateView(); ?>
</article>

