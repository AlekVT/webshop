<article>
    <form action="index.php" method="post">
        <nav id="command-panel" class="command-panel">
            <h1>Order UpdatingOne</h1>
            <div id="controls">
                <button type="submit" value="Order-updateOne" name="uc">
                    Update
                </button>
                <a href="index.php?uc=Order-editing">
                    Sluiten
                </a>
            </div>
        </nav>
        <div id="readingalles">
            <?php $partialView('Order', 'ReadingAll', $model); ?>
        </div>
            <fieldset id="content">
                <div>
                    <label for="Order-Comment">Opmerking:</label>
                    <input id="Order-Comment" name="Order-Comment"
                           class="text" type="text"
                           value="<?php echo $model->getComment(); ?>" required/>
                </div>
                <div>
                    <label for="Order-ShippingDate">Verzenddatum:</label>
                    <input type="text" id="Order-ShippingDate" name="Order-ShippingDate"
                        value="<?php echo $model->getShippingDate(); ?>"/>
                </div>
                <div>
                    <label for="Order-OrderDate">Besteldatum:</label>
                    <input type="text" id="Order-OrderDate" name="Order-OrderDate"
                        value="<?php echo $model->getOrderDate(); ?>"/>
                </div>
                <div>
                    <input id="Order-Id" name="Order-Id"
                           type="hidden" value="<?php echo $model->getId(); ?>" readonly/>
                </div>
                <div>
                    <label for="Order-IdCustomer">Klant:</label>
                    <select id="Order-IdCustomer" name="Order-IdCustomer" required
                        class="text" type="text">
                                <?php
                                if (count($model->getCustomers()) > 0) {
                                    foreach ($model->getCustomers() as $item) {
                                        ?><option value="<?php echo $item['Id'];?>"
                                                <?php 
                                                if($item['Id'] === $model->getIdCustomer()) { echo 'selected'; }
                                                ?>><?php echo $item['LastName'];?></option><?php
                                    }
                                } else {
                                    ?><option>Geen customers gevonden.</option><?php
                                } ?>
                    </select>
                </div>
                <div>
                    <label for="Order-IdShippingMethod">Verzendingsmethode:</label>
                    <select id="Order-IdShippingMethod" name="Order-IdShippingMethod" required
                        class="text" type="text">
                                <?php
                                if (count($model->getShippingList()) > 0) {
                                    foreach ($model->getShippingList() as $item) {
                                        ?><option value="<?php echo $item['Id'];?>"
                                                <?php 
                                                if($item['Id'] === $model->getIdShippingMethod()) { echo 'selected'; }
                                                ?>><?php echo $item['Name'];?></option><?php
                                    }
                                } else {
                                    ?><option>Geen shippingmethods gevonden.</option><?php
                                } ?>
                    </select>
                </div>
                <div>
                    <label for="Order-IdStatus">Status:</label>
                    <select id="Order-IdStatus" name="Order-IdStatus" required
                        class="text" type="text">
                                <?php
                                if (count($model->getStatusList()) > 0) {
                                    foreach ($model->getStatusList() as $item) {
                                        ?><option value="<?php echo $item['Id'];?>"
                                                <?php 
                                                if($item['Id'] === $model->getIdStatus()) { echo 'selected'; }
                                                ?>><?php echo $item['Name'];?></option><?php
                                    }
                                } else {
                                    ?><option>Geen status gevonden.</option><?php
                                } ?>
                    </select>
                </div>
                

                <h2>Order Items<button class="insorderitemsbtn" type="submit" value="OrderItem-CreatingOne_<?php echo $model->getId();?>" name="uc">
                        Insert OrderItem
                </button></h2> 
                <div id="readingorderitems">
                <table>
                <?php 
                    $fullprice = 0;
                    foreach($model->getOrderItems() as $item) 
                    {
                        if ($item['IdOrder'] === $model->getId()) 
                        { 
                            $quantity = $item['Quantity'];
                            $price = $item['Price'];
                            $fullprice = $fullprice + ($price * $quantity);
                        ?>
                        <tr>
                            <td>
                                <?php echo $item['ProductName'];?>
                            </td>
                            <td>
                                <?php echo "$$price/" . $item['UnitBaseName'];?>
                            </td>
                            <td>
                                <?php echo "$quantity " . $item['UnitBaseName'];?>
                            </td>
                            <td>
                                <button class="oitemsbtn" type="submit" value="OrderItem-UpdatingOne_<?php echo $item['Id'];?>" name="uc">
                                    Updating
                                </button>
                            </td>
                            <td>
                                <button class="oitemsbtn" type="submit" value="OrderItem-deleteOne_<?php echo $item['Id'];?>" name="uc">
                                    Delete
                                </button>
                            </td>
                        </tr>
                        <?php
                        }
                    }
                ?>
                        <tr>
                            <td><label>Totaal: <?php echo $fullprice . '$' ?></label></td>
                        </tr>
                </table>
                    
                
                </div>
            </fieldset>
    </form>
    <button class="log" onclick="toggle_visibility()">Show Log</button>
    <?php $appStateView(); ?>
</article>

