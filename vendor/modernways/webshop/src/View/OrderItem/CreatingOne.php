<article>   
    <form class="room" action="Index.php" method="post">
        <nav>
            <h1>OrderItem CreatingOne</h1>
            <div id="controls">
                <button type="submit" value="<?php 
                if ($model->getIdOrder() !== NULL) {
                    echo 'OrderItem-createNext';
                } else {
                    echo 'OrderItem-createOne';
                }?>" name="uc">
                    Opslaan
                </button>
                <a href="<?php
                    if ($model->getIdOrder() !== NULL) {
                        echo 'index.php?uc=Order-UpdatingOne_' . $model->getIdOrder();
                    } else {
                        echo 'index.php?uc=OrderItem-editing';
                    }
                ?>">
                    Sluiten
                </a>
            </div>
        </nav>
        <div id="readingalles">
            <?php $partialView('OrderItem', 'ReadingAll', $model); ?>
        </div>
        <div id="content">
            <fieldset>
                <div>
                    <label for="OrderItem-Quantity">Hoeveelheid</label>
                    <input id="OrderItem-Quantity" name="OrderItem-Quantity"
                       type="number" min="1" value="1">
                </div>
                
                <div>
                    <label for="OrderItem-IdOrder">Bestelling</label>
                    <?php if ($model->getIdOrder() !== NULL) {
                        ?><input id="OrderItem-IdOrder" name="OrderItem-IdOrder" required
                               class="text" type="text" readonly value="<?php echo $model->getIdOrder();?>"/><?php
                    }
                    else {?>
                        <select id="OrderItem-IdOrder" name="OrderItem-IdOrder" required
                            class="text" type="text">
                                    <?php
                                    if (count($model->getOrders()) > 0) {
                                        foreach ($model->getOrders() as $item) {
                                            ?><option value="<?php echo $item['Id'];?>"><?php echo $item['Id'];?></option><?php
                                        }
                                    } else {
                                        ?><option>Geen orders gevonden.</option><?php
                                    } ?>
                        </select>
                </div><?php }?>
                <div>
                    <label for="OrderItem-IdProduct">Product</label>
                    <select id="OrderItem-IdProduct" name="OrderItem-IdProduct" required
                        type="text">
                                <?php
                                if (count($model->getProducts()) > 0) {
                                    foreach ($model->getProducts() as $item) {
                                        ?><option value="<?php echo $item['Id'];?>"><?php echo $item['Name'];?></option><?php
                                    }
                                } else {
                                    ?><option>Geen producten gevonden.</option><?php
                                } ?>
                    </select>
                </div>
            </fieldset>
        </div>
    </form>
    <button class="log" onclick="toggle_visibility()">Show Log</button>
    <?php $appStateView(); ?>
</article>

