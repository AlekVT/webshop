
<article>
    
    <form action="index.php" method="post">
        <nav>
            <h1>OrderItem ReadingOne</h1>
            <div id="controls">
                <button type="submit" value="OrderItem-updatingOne" name="uc">
                    Updating
                </button>
                <button type="submit" value="OrderItem-CreatingOne" name="uc">
                    Inserten
                </button>
                <button type="submit" value="OrderItem-deleteOne" name="uc">
                    Delete
                </button>
                <button type="submit" value="OrderItem-editing" name="uc">
                    OrderItems
                </button>
                <button type="submit" value="Order-readingOne_<?php echo $model->getIdOrder();?>" name="uc">
                    Order
                </button>
            </div>
        </nav>
        <div id="readingalles">
            <?php $partialView('OrderItem', 'ReadingAll', $model); ?>
        </div>
        <fieldset id="content">
            <div>
                <label for="OrderItem-Id">Id:</label>
                <input id="OrderItem-Id" name="OrderItem-Id" class="text" type="text" value="<?php echo $model->getId();?>" readonly />
            </div>
            <div>
                <label for="OrderItem-Count">Hoeveelheid:</label>
                <input type=text" id="OrderItem-Count" name="OrderItem-Count" readonly value="<?php echo $model->getQuantity();?>"/>
            </div>
            <div>
                <label for="OrderItem-IdProduct">Product:</label>
                <input type="text" id="OrderItem-IdProduct" name="OrderItem-IdProduct"readonly value="<?php echo $model->getProductName(); ?>"/>
            </div>
        </fieldset>
    </form>
    <button class="log" onclick="toggle_visibility()">Show Log</button>
    <?php $appStateView(); ?>
</article>
