<article>
    <form class="room" action="index.php" method="post">
        <nav>
            <h1>OrderItem UpdatingOne</h1>
            <div id="controls">
                <button type="submit" value="OrderItem-updateOne" name="uc">
                    Update
                </button>
                <button type="submit" value="<?php 
                    echo 'OrderItem-ReadingOne_' . $model->getId();
                ?>" name="uc">
                    OrderItem
                </button>
                <button type="submit" value="<?php 
                    echo 'Order-UpdatingOne_' . $model->getIdOrder();
                ?>" name="uc">
                    Order
                </button>
            </div>
        </nav>
        <div id="readingalles">
            <?php $partialView('OrderItem', 'ReadingAll', $model); ?>
        </div>
        <fieldset id="content">
            <div>
                <input id="OrderItem-Id" name="OrderItem-Id"
                       type="hidden" value="<?php echo $model->getId(); ?>" readonly/>
            </div>
            <div>
                <label for="OrderItem-Quantity">Hoeveelheid</label>
                <input id="OrderItem-Quantity" name="OrderItem-Quantity"
                       type="number" value="<?php echo $model->getQuantity();?>" />
            </div>
            <div>
                <label for="OrderItem-IdOrder">Bestelling</label>
                <select id="OrderItem-IdOrder" name="OrderItem-IdOrder" required
                    class="text" type="text">
                    <?php
                    if (count($model->getOrders()) > 0) {
                        foreach ($model->getOrders() as $item) {
                            ?><option value="<?php echo $item['Id'];?>" 
                                <?php 
                                if($item['Id'] === $model->getId()) { echo 'selected'; }
                                ?>><?php echo $item['Id'];?></option><?php
                        }
                    } else {
                        ?><option>Geen orders gevonden.</option><?php
                    } ?>
                </select>
            </div>
            <div>
                <label for="OrderItem-IdProduct">IdProduct</label>
                <select id="OrderItem-IdProduct" name="OrderItem-IdProduct" required
                    class="text" type="text">
                            <?php
                            if (count($model->getProducts()) > 0) {
                                foreach ($model->getProducts() as $item) {
                                    ?><option value="<?php echo $item['Id'];?>" 
                                        <?php 
                                        if($item['Id'] === $model->getIdProduct()) { echo 'selected'; }
                                        ?>><?php echo $item['Name'];?></option><?php
                                }
                            } else {
                                ?><option>Geen producten gevonden.</option><?php
                            } ?>
                </select>
            </div>
        </fieldset>
    </form>
    <button class="log" onclick="toggle_visibility()">Show Log</button>
    <?php $appStateView(); ?>
</article>

